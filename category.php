<?php
	//load template header file
	get_header();
?>
        
        <?php get_sidebar();?>

        
        <div class="article-section">
            <h1>PREPARED FOOD</h1>
            <h2> Food</h2>

            
            
            <?php while(have_posts()):?>
            <div class="individual-article">
            
            <?php the_post();?>

                <?php the_post_thumbnail();?>
                <h3><?php the_title();?></h3>
                <p><?php echo $post->post_content;?></p>
                <a href="<?php the_permalink();?>">Learn more >></a>
                
            </div>
            <?php endwhile;?>

        </div>

      </div><!--Content div end-->

<?php
  //load template footer file
  get_footer();
?>
