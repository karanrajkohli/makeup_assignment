<?php get_header(); ?>

			<div id="primary">

				<h1>404 Error - Page not found</h1>

				<h2>Sorry for the inconvenience</h2>

				<p>Please use our navigation to go to the page you are looking for.</p>
			</div>
<?php get_footer(); ?>
