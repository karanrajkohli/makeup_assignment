<?php


if(function_exists('add_theme_support')){
    add_theme_support('post-thumbnails');
    add_image_size('list-thumb', 75, 75);
  }
  
  if(function_exists('register_nav_menus')){
    register_nav_menus();
  }
  wp_enqueue_style( 'style', get_stylesheet_uri() );