<?php
	//load template header file
	get_header();
?>
<?php get_sidebar();?>
<div class="article-section-single">
<?php while(have_posts()):?>


<div class="individual-article-single">
<?php the_post();?>
<?php the_post_thumbnail();?>
                <h3><?php the_title();?></h3>
                <p><?php echo $post->post_content;?></p>
                <?php endwhile;?>
            </div>
            
        </div>

      </div><!--Content div end-->
<?php
	//load template footer file
	get_footer();
?>
