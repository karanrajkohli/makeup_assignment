      <footer class="footer"><!--Footer start-->
        
        <div class="footer-logo-left">
          <img src="<?=get_template_directory_uri()?>/Images/footer-logo.png" alt="mini white dog logo">
        </div>

        <div class="footer-content">
            <?php wp_nav_menu(['menu'=>'footer-1'])?>
            <?php wp_nav_menu(['menu'=>'footer-2'])?>

            <p class="copyright">Contents copyright (c) 2010 - 2014 by Westland Partners</p>
        </div>

        <div class="footer-logo-right">
          <img src="<?=get_template_directory_uri()?>/Images/Wordmark.png" alt="mini wordmark logo">
        </div>

      </footer><!--Footer end-->

    </div><!--Wrapper div end-->
    <?php wp_footer()?>
  </body>
  </html>