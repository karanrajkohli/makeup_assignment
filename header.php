<!doctype html>
<html lang="en">
  <head>
    <title>Dog Food Assignment</title>
    <meta charset="utf-8">
		<style>
		
        </style>
        <?php wp_head()?>
	
  </head>


  <body>
    <div id="wrapper"><!--Wrapper div start-->
      <header><!--Utility div start-->
        <div id="utility-nav">
          <div class="uti-icons">
            <a href="#"><img src="<?=get_template_directory_uri()?>/Images/fb.png" alt="Facebook icon" style="margin-right:5px;"></a>
            <a href="#"><img src="<?=get_template_directory_uri()?>/Images/twitter.png" alt="Twitter icon"></a>
          </div>
          <div class="uti-links">
            <?php wp_nav_menu(['menu'=>'uti-nav'])?>
          </div>
        </div>
      </header><!--Utility div end-->

      <div id="content"><!--Content div start-->

        <div id="main-header"><!--Main Header section div start-->

          <div id="logo-container" style="background-image: url("<?=get_template_directory_uri()?>/Images/footer-logo.png");">
            <img src="<?=get_template_directory_uri()?>/Images/logo.png" alt="Dog food logo">
          </div>

          <div id="header-image-container">
            <img src="<?=get_template_directory_uri()?>/Images/header-image.png" alt="Header Image of a boy and its dog">
          </div>

          <nav>
          <?php wp_nav_menu(['menu'=>'main-nav'])?>
          </nav>
        </div><!--Main Header section div end-->