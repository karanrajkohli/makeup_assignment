<?php
	//load template header file
	get_header();
?>
        <div id="hero">
          <img class="food" src="<?=get_template_directory_uri()?>/Images/Fodo-bag.png" alt="Dog food bag image">
          <h1>Hungry for life</h1>
          <h2>New formula available now.</h2>

          <ul class="hero-list">
            <li>- Improved fur glossiness</li>
            <li>- Stronger teeth</li>
            <li>- Brighter eyes</li>
            <li>- Better breath</li>
            <li>- More energy</li>
          </ul>

          <img class="dog-image" src="<?=get_template_directory_uri()?>/Images/dog.png" alt="just a brown dog">

          <a class="order-button" href="#">Order today!</a>
        </div><!--hero div end-->

        <div id="articles-container"><!--Article section div start-->
        <?php $posts = get_posts(['category_name'=>'home']);?>
        <?php foreach($posts as $post): ?>
          <div class="article">
            <?php the_post_thumbnail();?>
            <h3><?php echo get_the_title();?></h3>
            <p class="content"><?php echo $post->post_content;?></p>
            <a href="<?php echo get_the_permalink();?>">Read more ></a>
          </div>

         
          <?php endforeach;?>
        </div><!--Article section div end-->
        
      </div><!--Content div end-->


<?php
  //load template footer file
  get_footer();
?>
